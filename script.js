var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { x: 0,
                        y: 0 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};
const margin = 20;

var game = new Phaser.Game(config);
let player1;
let player2;
let lifeP1 = 100;
let lifeP2 = 100;
let round;
let roundP1 = 0;
let roundP2 = 0;
let gameOver= false;
starPosX1= [360,340,320];
starPosX2= [410,430,450];
let showLifeP1;
let showLifeP2;
let cursors;
let keyA;
let keyE;
let keyD;
let keyS;
let keyF;
let keyEnter;
let animLazer;
let split;
let lazer;
let lazer2;
let lazerGroup;
let fireLazer;
let bonus;
let interval;
let speed1= 100;
let speed2= 100;
let shoot1= 500;
let shoot2= 500;
let textBonus1;
let textBonus2;
objetBonus={
    player:'',
    bonus:'',
    value:'',
    text: ''
}
bonusArray=[["speedUp",150, 'Speed \u2795'],["speedDown",50, 'Speed	\u2796'],["shootUp",700, 'Shoot \u2795'],["shootDown",250, 'Shoot \u2796']];
let music;

let textConfig = {
            x: 300,
            y: 200,
            text: '',
            style: {
                font: '64px Arial',
                fill: '#ffffff',
                align: 'center',
                backgroundColor: 'red'
        }
}

function preload ()
{

    this.load.image('player', 'assets/games/defender/ship.png');
    this.load.image('player2', 'assets/games/defender/ship2.png');
    this.load.image('lazer97', 'assets/games/defender/lazer97.png');
    this.load.image('lazer973', 'assets/games/defender/lazer973.png');
    this.load.image('star', 'assets/demoscene/star2.png');
    this.load.image('baddie', 'assets/sprites/space-baddie.png');
    this.load.image('split', 'assets/games/defender/split.png');
    this.load.image('bonus', 'assets/games/defender/bonus.png');
    this.load.spritesheet('boom', 'assets/sprites/explosion.png', { frameWidth: 64, frameHeight: 64, endFrame: 23 });
    this.load.audio('badam', ['assets/audio/badam.ogg', 'assets/audio/badam.mp3']);
    this.load.audio('piou1', ['assets/audio/piou1.ogg', 'assets/audio/piou1.mp3']);
    this.load.audio('piou2', ['assets/audio/piou2.ogg', 'assets/audio/piou2.mp3']);
    this.load.audio('gameover', ['assets/audio/gameover.ogg', 'assets/audio/gameover.mp3']);
    this.load.audio('space_bullet', ['assets/audio/space_bullet.ogg', 'assets/audio/space_bullet.mp3']);


}

function create ()
{

        music = this.sound.add('space_bullet');

        platforms = this.physics.add.staticGroup();

        
        randomBonus = this.physics.add.group();
        this.physics.add.collider(randomBonus, platforms);

        var config = {
            key: 'explode',
            frames: this.anims.generateFrameNumbers('boom', { start: 0, end: 23, first: 23 }),
            frameRate: 20
        };

        this.anims.create(config);

        /**
         * Disposition des étoiles dans la scène
         */
        for (var i = 0; i < 128; i++)
        {
            let rand = Math.random() * (800 - 1);
            let rand2 = Math.random() * (800 - 1);

            this.add.image(rand, rand2, 'star');
        }

        cursors = this.input.keyboard.createCursorKeys();

        player1 = this.physics.add.image(100, 250, 'player'); // ajoute le vaisseau du joueur 1
        player2 = this.physics.add.image(700, 250, 'player2'); // ajoute le vaisseau du joueur 2

        keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        keyE = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E);
        keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        keyS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        keyF = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F);
        keyEnter = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ENTER);

        this.add.text(760, 10, '\u2764\ufe0f', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' });     // emoji coueur joueur 2
        this.add.text(10, 10, '\u2764\ufe0f', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' });      // emoji coeur joueur 1

        textBonus1 = this.add.text(90, 10, '', { fontFamily: 'Arial', fontSize: 20, color: '#ffff00' });  // bonus joueur 1
        textBonus2 = this.add.text(620, 10, '', { fontFamily: 'Arial', fontSize: 20, color: '#ffff00' });  // bonus joueur 2


        for (var j = 0; j < roundP2; j++) {
            this.add.text(starPosX2[j], 10, '\u2b50', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' }); 
        }
       
        for (var i = 0; i < roundP1; i++) {
           
            this.add.text(starPosX1[i], 10, '\u2b50', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' });
           
        }
        
        showLifeP1 = this.add.text(40, 10, lifeP1, { fontFamily: 'Arial', fontSize: 20, color: '#00ff00' });
        showLifeP2 = this.add.text(725, 10, lifeP2, { fontFamily: 'Arial', fontSize: 20, color: '#00ff00' });

        split = this.add.image(400,300, 'split');

        fireLazer = this.physics.add.group();

        this.physics.add.collider(player1, fireLazer, hitLazer1, null, this);
        this.physics.add.collider(player2, fireLazer, hitLazer2, null, this);
        this.physics.add.collider(randomBonus, fireLazer, hitLazer3, null, this);
        
        
        interval = setInterval(function(){
            var x = Phaser.Math.Between(0, 800);
            var y = Phaser.Math.Between(0, 600);
            bonus = randomBonus.create(x, y, 'bonus');
            bonus.setBounce(1);
            bonus.setCollideWorldBounds(true);
            bonus.setVelocity(300);
        }, 25000)

        
        music.play({
            loop:true
        });
}



function update()
{

    
    
    if (!gameOver) {
         if(round)
        {
            for (var j = 0; j < roundP2; j++) {

                this.add.text(starPosX2[j], 10, '\u2b50', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' }); 
            }

           
            for (var i = 0; i < roundP1; i++) {

                this.add.text(starPosX1[i], 10, '\u2b50', { fontFamily: 'Arial', fontSize: 20, fill: '#ff0000' });
            }
            

            textConfig.text= 'restart';
            let text = this.make.text(textConfig);

            this.input.on('pointerup', () =>{

                lifeP1 = 100;
                lifeP2 = 100;

                this.scene.restart();

            });

            round = false;

        }
    }else{
        textConfig.text= 'GAME \nOVER';
        clearInterval(interval);
        let text = this.make.text(textConfig);
        this.input.on('pointerup', () =>{

            roundP1 = 0;
            roundP2 = 0;
            lifeP1 = 100;
            lifeP2 = 100;

            this.scene.restart();

        });
        gameOver= false;
        return;

    }

       if(objetBonus.player == 'lazer97' && objetBonus.bonus.slice(0,5) == 'speed')
       {
        speed1 = objetBonus.value;
        textBonus1.text = objetBonus.text;
  
       
       }

       if(objetBonus.player == 'lazer97' && objetBonus.bonus.slice(0,5) == 'shoot')
       {
        shoot1 = objetBonus.value;
        textBonus1.text = objetBonus.text;  

       
       }

       if(objetBonus.player == 'lazer973' && objetBonus.bonus.slice(0,5) == 'shoot')
       {
        shoot2 = objetBonus.value;
        textBonus2.text = objetBonus.text;  
      
       }
       if(objetBonus.player == 'lazer973' && objetBonus.bonus.slice(0,5) == 'speed')
       {
        speed2 = objetBonus.value;
        textBonus2.text = objetBonus.text;  
      
       }


    if (keyS.isDown)
    {
        player1.setVelocityX(-speed1);
    }
    else if (keyF.isDown)
    {
        player1.setVelocityX(speed1);
    }
    else
    {
        player1.setVelocityX(0);
    }
    if(Phaser.Input.Keyboard.JustDown(keyA))
    {
        fireBulletP1(this);
    }

    if (keyE.isDown)
    {
        player1.setVelocityY(-speed1);

    }
    else if (keyD.isDown)
    {
        player1.setVelocityY(speed1);
    }
    else
    {
        player1.setVelocityY(0);
    }


    if(player1.x > 380)
    {
        player1.x = 380;
    }
    else if(player1.x < 20)
    {
        player1.x = 20;
    }
    else if(player1.y < 10)
    {
        player1.y = 10;
    }
    else if(player1.y > 590)
    {
        player1.y = 590;
    }


    if(player2.x < 420)
    {
        player2.x = 420;
    }
    else if(player2.x > 780)
    {
        player2.x = 780;
    }
    else if(player2.y < 10)
    {
        player2.y = 10;
    }
    else if(player2.y > 590)
    {
        player2.y = 590;
    }


    if (cursors.left.isDown)
    {
        player2.setVelocityX(-speed2);

    }
    else if (cursors.right.isDown)
    {
        player2.setVelocityX(speed2);
    }
    else
    {
        player2.setVelocityX(0);
    }

    if(Phaser.Input.Keyboard.JustDown(keyEnter))
    {
         fireBulletP2(this);
    }

    if (cursors.up.isDown)
    {
        player2.setVelocityY(-speed2);

    }
    else if (cursors.down.isDown)
    {
        player2.setVelocityY(speed2);
    }
    else
    {
        player2.setVelocityY(0);
    }

}

function fireBulletP1(scene)
{
   lazer = fireLazer.create((player1.x + 20), player1.y, 'lazer97');
   lazer.setVelocityX(shoot1);

   var piou2 = scene.sound.add('piou2');
   piou2.play();
   
}

function fireBulletP2(scene)
{
   lazer = fireLazer.create((player2.x -20), player2.y, 'lazer973');
   lazer.setVelocityX(-shoot2);

   var piou1 = scene.sound.add('piou1');
   piou1.play();
   
}

function hitLazer1(player1, lazer)
{
     
    if(lifeP1 <= 10)
    {
        var boom = this.add.sprite(player1.x, player1.y, 'boom');
        boom.anims.play('explode');
        var badam = this.sound.add('badam');
        badam.play();

        player1.x = 1000;

        lifeP1-=10;
        showLifeP1.text = lifeP1;
        clearInterval(interval);
        roundOver(this);
        return;
    }


    lifeP1-=10;
    showLifeP1.text = lifeP1;
    lazer.destroy();

}

function hitLazer2(player2, lazer)
{
   

    if(lifeP2 <= 10)
    {
        var boom = this.add.sprite(player2.x, player2.y, 'boom');
        boom.anims.play('explode');
        var badam = this.sound.add('badam');
        badam.play();

        player2.x = -1000;

        lifeP2-=10;
        showLifeP2.text = lifeP2;
        clearInterval(interval);
        roundOver(this);
        return;
    }

    lifeP2-=10;
    showLifeP2.text = lifeP2;
    lazer.destroy();
}

function roundOver(scene)
{
    music.stop();
    if((roundP1 + roundP2) < 2){

        round = true;
        if(lifeP1 == 0)
        {
            player1.alpha = 0;
            roundP2++;

        }
        else if(lifeP2 == 0)
        {
            player2.alpha = 0;
            roundP1++;
        }
        scene.physics.pause();
        return;
    }
    else{
        GameOver(scene)
        return
    }
}

function GameOver(scene){

    music.stop();
    var gameover = scene.sound.add('gameover');
    gameover.play();
    scene.physics.pause();
    gameOver= true;
}

function RandomBonus (laser)
{
    let randNum = Phaser.Math.Between(0, 3);
    let player = laser.texture.key;
    objetBonus.player = player;
    objetBonus.bonus= bonusArray[randNum][0];
    objetBonus.value= bonusArray[randNum][1];
    objetBonus.text=bonusArray[randNum][2];

}


function hitLazer3(bonus, lazer)
{
    bonus.destroy();
    lazer.destroy();
    RandomBonus(lazer)
}